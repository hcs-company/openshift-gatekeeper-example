# Limiting what OpenShift Projects can be Created

On OpenShift any user with a clusterRolebinding to the `self-provisioner`
clusterRole is allowed to create new projects, as long as they don't start with
a restricted prefix (i.e. `openshift` or `kube`). This is done with the `oc
new-project` command or a `ProjectRequest` object, as regular users are not
allowed to touch namespaces directly.

In a multi-tenant environment this can cause issues where people create new
Projects (namespaces) with seemingly random names, making it ahrd to track what
project belongs that what team.

One solution is to limit self-provisioning to only trusted users, but this
tends to get in the way of self-service.

## This Solution

One way to tackle this problem is to allow (a select set of) users to still
self-provision, but to set extra limits on the names of the projects they
create.

This repository contains an Open Policy Agent GateKeeper ConstraintTemplate,
and an example Constraint, that only allows project creation if at least on of the following conditions is true:
- The project name must start with name of the requesting user, followed by a
  `-` and some other characters.
- The project name starts with the short name of one of the groups of the
  requesting user. For example, if the requesting user is part of the `foo` and
  `example-project` groups projects that start with `foo-` and `example-` are
  allowed. Notice how group names containing dashes are trimmed to the part
  before the first dash.
- ServiceAccounts with self-provisioner access (for example a serviceaccount used by a pipeline or ArgoCD instance) can create projects if both of these conditions are true:
  - The ServiceAccount is from a namespace that ends with a dash followed by one of the values specified in the `allowedParentPostfixes` setting of the Constraint
  - The new project starts with the same prefix as the project in which the
    requesting ServiceAccounts lives.
  For example, take a Constraint set up as follows:
  ```yaml
  apiVersion: constraints.gatekeeper.sh/v1beta1
  kind: OpenShiftAllowedProjects
  metadata:
    name: openshiftallowedprojects
  spec:
    match:
      kinds:
      - apiGroups:
        - project.openshift.io
        kinds:
        - ProjectRequest
    parameters:
      allowedParentPostfixes:
      - cicd
  ```
  In this example a ServiceAccount from the `example-cicd` is allowed to create
  new projects that start with `example-`, but a ServiceAccount from the
  namespace `example-dev` is not allowed to create new projects, even if it has
  self-provisioner access.

## Deploying This Example

The `deploy` directory contains a working set of Kustomizations that can be
deployed using ArgoCD, or directly using `kustomize`/`oc`/`kubectl`. When
deploying from the command line please keep in mind that some of the manifests
have dependencies on each other:
- ##### `subscription.yaml`
  This manifest deploys the Open Policy Agent Gatekeeper operator on your
  cluster. **N.B.** This does not deploy Gatekeeper itself.
- ##### `gatekeeper.yaml`
  This manifest deploys an instance of Gatekeeper. Deploying this will fail until the operator is healthy enough to have created the CustomResourceDefinitions for Gatekeeper.
- ##### `constrainttemplate.yaml`
  This manifest deploys the example ConstraintTemplate. This can only be deployed when Gatekeeper has been deployed
- ##### `constraint.yml`
  This manifest deploys the actual constraint what will enforce across your cluster.
